# Change Log

## v4.0.0

Please read the [migration guide](./docs/migrating.md) for information on how to update to v4.

* [BREAKING] Removes Sass from default configuration
* [BREAKING] Removes default PostCSS configuration to allow projects to configure it themselves
* [BREAKING] Updates from Webpack v4 to v5
* [BREAKING] Sets `webpack-dev-middleware` and `webpack-hot-middleware` as optional dependencies
* Adds Yarn 2 PnP support
* Updates all dependencies
* Switches from `@vizworx/babel-preset` (deprecated) to `@babel/preset-env`
* Sets optional peer dependencies
* Changes from `[hash]` to `[contenthash]` - See Webpack's [Output - Template Strings](https://webpack.js.org/configuration/output/#template-strings) for more details
* Switches from `url-loader` and `file-loader` to [Asset Modules](https://webpack.js.org/guides/asset-modules/) format, as they are deprecated in Webpack v5
* Limits [git-revision-webpack-plugin](https://github.com/pirelenito/git-revision-webpack-plugin) to only run in a Git repository
* Adds example project of usage
