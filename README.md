# Webpack Starter

This monorepo contains everything required to develop VizworX' Webpack Starter, a set of sane defaults that can be used whole or as individual parts. Please see the [`@vizworx/webpack-starter` docs](./packages/webpack-starter/README.md) for more details.
