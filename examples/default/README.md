# `@vizworx/webpack-starter` - Examples - Default Functionality

This provides an example of how to use `@vizworx/webpack-starter` with either the Webpack DevServer or with the Express middleware.

- The example is setup to use the built `webpack-starter` package `(cd ../../packages/webpack-starter/; yarn build)`
