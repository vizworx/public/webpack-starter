import React from 'react';
import './styles.css';

const PostCSS = () => (
  <>
    <h3>PostCSS - These should appear side-by-side</h3>
    <div className="postcss-top">
      <div className="nested" />
      <div className="nested blue" />
      <div className="nested" />
    </div>
  </>
);

export default PostCSS;
