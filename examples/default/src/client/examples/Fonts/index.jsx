import React from 'react';
import './styles.css';

const Fonts = () => (
  <>
    <h2>Fonts</h2>
    <h3 style={{ fontFamily: 'Roboto' }}>Hello Roboto</h3>
  </>
);

export default Fonts;
