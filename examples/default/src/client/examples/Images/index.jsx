import React from 'react';
import gif from './example.gif';
import jpg from './example.jpg';
import jpeg from './example.jpeg';
import png from './example.png';
import ReactSVG from './example.svg';
import './styles.css';

const Images = () => (
  <>
    <h2>Images</h2>
    <h3>gif</h3>
    <img src={gif} />
    <h3>jpg</h3>
    <img src={jpg} />
    <h3>jpeg</h3>
    <img src={jpeg} />
    <h3>png</h3>
    <img src={png} />
    <h3>svg</h3>
    <div className="images-svg-background" />
    <h3>SVG as React component</h3>
    <ReactSVG width="512" />
  </>
);

export default Images;
