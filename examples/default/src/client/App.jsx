import React from 'react';
import Fonts from './examples/Fonts';
import Images from './examples/Images';
import PostCSS from './examples/PostCSS';

const App = () => (
  <>
    <h1>Examples</h1>
    <Fonts />
    <Images />
    <PostCSS />
  </>
);

export default App;
