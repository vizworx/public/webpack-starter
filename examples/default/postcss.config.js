const postCSSPresetEnv = require('postcss-preset-env');

module.exports = {
  plugins: [
    postCSSPresetEnv({
      autoprefixer: {
        flexbox: 'no-2009',
      },
    }),
    require('postcss-nested'),
  ],
};
