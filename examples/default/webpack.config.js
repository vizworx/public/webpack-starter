const path = require('path');
const webpackStarter = require('@vizworx/webpack-starter').default;

module.exports = webpackStarter({
  html: { title: 'Example - Default Functionality' },
  entry: './src/client/index.jsx',
  output: {
    path: path.resolve(__dirname, 'dist/client'),
  },
});
