# Webpack Starter

Setting up a fresh Webpack configuration can be overwhelming, even when following a start-to-finish guide like [SurviveJS - Webpack](https://survivejs.com/webpack/foreword/). We have compiled a set of sane defaults that can be customized in order to bootstrap new and existing projects.

> If you are upgrading to v4 or v5, please see our [migration guide](docs/migrating.md).

## Minimal Setup

To start using this project, install the package and set up your `webpack.config.babel.js` to customize the default configuration.

```sh
npm install --save-dev @vizworx/webpack-starter
```

```sh
npm install --save-dev @pmmmwh/react-refresh-webpack-plugin react-refresh
```

### webpack.config.babel.js

```js
import webpackStarter from '@vizworx/webpack-starter';

export default webpackStarter({
  html: { title: 'My Webpack Project' },
});
```

## Configuration

Many of the [parts](#parts) can be configured through the `webpackStarter` function, using the options listed in their section.

```js
import webpackStarter from '@vizworx/webpack-starter';

export default webpackStarter({
  html: {
    title: 'My Webpack Project'
    meta: {
      robots: 'noindex, nofollow',
    },
  },
  react: true,
  hot: true,
  development: {
    port: 3000,
  },
  production: {
    lazyChunks: true,
  },
});
```

If you pass `{ react: false }` or `{ hot: false }`, you do not need the `@pmmmwh/react-refresh-webpack-plugin` or
`react-refresh` dependencies.

## Webpack Middleware

If you are using the `webpack-dev-middleware` with Express, you can use `addStarterMiddleware` to automatically add the appropriate middleware and hot reload support.

```js
import express from 'express';
import path from 'path';

const app = express();
const publicFolder = path.resolve(__dirname, '../client');
app.use(express.static(publicFolder));

if (process.env.NODE_ENV !== 'development') {
  app.get('*', (req, res) => res.sendFile(path.resolve(publicFolder, 'index.html')));
} else {
  /* eslint-disable global-require, import/no-extraneous-dependencies */
  // To prevent these from loading in production, we need to use a scoped require
  const { addStarterMiddleware } = require('@vizworx/webpack-starter');
  const webpackConfig = require('../../webpack.config.babel').default;
  addStarterMiddleware(app, webpackConfig);
  /* eslint-enable global-require, import/no-extraneous-dependencies */
}

// eslint-disable-next-line no-console
app.listen(3000, () => console.log(`Listening on http://localhost:3000 (${process.env.NODE_ENV})`));
```

### Routing

This option will register `GET *` (or `GET yourEntryPointFolder/*` if you have multiple entrypoints) with the Express app to serve Webpack's generated `index.html`. Any other GET routes **must** be registered prior to running `addStarterMiddleware` as any GET route added afterward will be inaccessible.

## Parts

The configuration has been split into multiple parts, that can be used via the `webpackStarter` function, or used manually by passing an array of parts into `webpack-merge` ([SurviveJS - Webpack](https://survivejs.com/webpack/developing/composing-configuration/) explains this in detail).

```js
import merge from 'webpack-merge';
import * as parts from '@vizworx/webpack-starter';

export default merge([
  parts.common(),
  parts.babel({ react: false }),
  parts.html({ title: 'Hello World' }),
  (process.env.NODE_ENV ==='development')
    ? parts.development({ port: 3000 })
    : parts.production(),
]);
```

### [babel](https://gitlab.com/vizworx/public/webpack-starter/blob/master/src/webpack/babel.js)

The `babel` part will set up [babel-loader](https://www.npmjs.com/package/babel-loader) to process `.js` files, and can optionally add support for `.jsx` as well.

#### Options

* `react` (`boolean`): Enables the `.jsx` extension
* `hot` (`boolean`): Enables hot-reloading of React
* `fullySpecified` (`boolean`: `false`): [Require modules to fully specify extensions](https://webpack.js.org/configuration/module/#resolvefullyspecified)
* `babel` (`object`): [Options that are passed to `babel-loader`](https://github.com/babel/babel-loader#option)

### [common](https://gitlab.com/vizworx/public/webpack-starter/blob/master/src/webpack/common.js)

The `common` part will set up a default entrypoint, output names, extensions, and the [CaseSensitivePathsPlugin](https://www.npmjs.com/package/case-sensitive-paths-webpack-plugin).

### [development](https://gitlab.com/vizworx/public/webpack-starter/blob/master/src/webpack/development.js)

The `development` part will set the Webpack [mode](https://webpack.js.org/configuration/mode/) to `development`, enable the (cheap-module-eval-source-map devtool)[https://webpack.js.org/configuration/devtool/], and enable the [devServer](https://webpack.js.org/configuration/dev-server/).

All options that are provided will be passed through to the [devServer](https://webpack.js.org/configuration/dev-server/). The default is to use the HOST and PORT environment variables and to rewrite all missing paths to `/index.html` to support Single Page Applications.

### [fonts](https://gitlab.com/vizworx/public/webpack-starter/blob/master/src/webpack/fonts.js)

The `fonts` part will automatically process `woff`, `woff2`, `ttf`, `otf`, and `eot`. Any files smaller than 8KB will be converted to Base64 and inlined, and the rest will be output in the `fonts` folder.

#### Options

* `staticPath` (`string`): The path (without the leading slash) that the fonts should be loaded from (eg. `static`)

### [gitRevision](https://gitlab.com/vizworx/public/webpack-starter/blob/master/src/webpack/gitRevision.js)

The `gitRevision` part will inject the current git commit hash (and tag if there is one) into the header of every generated chunk. This simplifies the process of associating a compiled chunk to the code it was built from. This can be disabled by passing `{ gitRevision: false }` in the webpackStarter argument.

### [html](https://gitlab.com/vizworx/public/webpack-starter/blob/master/src/webpack/html.js)

The `html` part uses a [tweaked version](https://gitlab.com/vizworx/public/webpack-starter/blob/master/src/static/html.ejs) of the [html-webpack-plugin](https://github.com/jantimon/html-webpack-plugin) template.

#### Options

In addition to the standard [html-webpack-plugin options](https://github.com/jantimon/html-webpack-plugin#options), you can also use:

* `extraHead` (`string` or `[string]`): HTML to add into the `<head>` of the template

* `extraBody` (`string` or `[string]`): HTML to add to the end of the `<body>` of the template

### [images](https://gitlab.com/vizworx/public/webpack-starter/blob/master/src/webpack/images.js)

The `images` part will add support for loading `svg`, `gif`, `jpg`, `jpeg`, and `png` from JS and CSS. If the file is smaller than 8KB, it will be converted to Base64 and inlined.

#### Options

* `staticPath` (`string`): The path (without the leading slash) that the images should be loaded from (eg. `static`)

### [production](https://gitlab.com/vizworx/public/webpack-starter/blob/master/src/webpack/production.js)

The `production` part will set the Webpack [mode](https://webpack.js.org/configuration/mode/) to `production`, and enable the [CleanWebpackPlugin](https://www.npmjs.com/package/clean-webpack-plugin) and [ManifestPlugin](https://www.npmjs.com/package/webpack-manifest-plugin). It can also set up some common defaults for [code-splitting](https://webpack.js.org/guides/code-splitting/) and [lazy-loading](https://webpack.js.org/guides/lazy-loading/) chunks.

#### Options

* `lazyChunks` (`boolean` or `object`): This will enable some generic vendor chunk optimizations for code splitting and lazy loading. It assumes that you have set up multiple code-splits and will attempt to keep async vendors in a shared file.
  * `minSize` (`number`): The minimum size (bytes) required before a new vendor chunk will be created
  * `maxSize` (`number`): The maximum size (bytes) that a vendor chunk may be before it is split in half
* `clean` (`object`): This will be passed to [clean-webpack-plugin](https://www.npmjs.com/package/clean-webpack-plugin)
* `manifest` (`object`): This will be passed to [webpack-manifest-plugin](https://www.npmjs.com/package/webpack-manifest-plugin)

### [react](https://gitlab.com/vizworx/public/webpack-starter/blob/master/src/webpack/react.js)

The `react` part adds support for hot-reloading components optionally with hooks, the react flag is set to true by default.

#### Options

* `entry`: See [Entry Points](https://webpack.js.org/concepts/entry-points/) for options
* `hot` (`boolean` or `object`): Enables the hot-reloading option and adds hooks support for hot-reloading.
  To enable this option make sure to:
    1. `npm install --save-dev @pmmmwh/react-refresh-webpack-plugin react-refresh`
    2. Include the `fast-refresh/babel` plugin only in development.

```js
const isDevelopment = process.env.NODE_ENV !== 'production';

module.exports = {
  presets: ['@babel/preset-env', '@babel/preset-react'],
  plugins: isDevelopment ? ['react-refresh/babel'] : [],
};
```

### [reactSVG](https://gitlab.com/vizworx/public/webpack-starter/blob/master/src/webpack/reactSVG.js)

The `reactSVG` part will allow JS/JSX files to automatically convert `svg` files into React components, via [@svgr/webpack](https://npmjs.com/package/@svgr/webpack).

### [styles](https://gitlab.com/vizworx/public/webpack-starter/blob/master/src/webpack/styles.js)

The `styles` part will add support for CSS, PostCSS, and optionally Sass, for both development and production. It does not provide a PostCSS configuration by default, allowing you to add your own `postcss.config.js`.

#### Options

* `extract` (`boolean`): Should the compiled CSS be extracted into a `css` file that is added to the `<head>`
* `postcss` (`boolean` or `object`: `true`): Should PostCSS be used. If passed an object, it will be passed to the [postcss-loader](https://www.npmjs.com/package/postcss-loader)
* `sass` (`boolean` or `object`: `false`): Should Sass be used to compile `.sass`, `.scss`, and `.css` files. If passed an object, it will be passed to the [sass-loader](https://www.npmjs.com/package/sass-loader), which you will need to install.
