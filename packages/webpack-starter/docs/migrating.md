# Migrating from v4 to v5

Webpack Starter v5 removes support for react-hot-loader to simplify support for React 18 and beyond going forward. This migration is simpler than the v4 migration by far. This guide will provide you with a steps to help you upgrade your application so that you can move to React 18. An example of the migration can be found in commit SHA [0e186fabe](webpack-starter@0e186fabe). There is one important highlight to be aware of with `fast-refresh`.

> For `fast-refresh` to function, your exported components **_must_** have a named export as shown in the code snippet below. This follows recommendations by the React community as highlighted by Dan Abramov [here](https://twitter.com/dan_abramov/status/1255229440860262400).
>
> Failing to do this will cause changes in components to trigger a full reload of the page rather than hot reloading. Details about this are in the [troubleshooting](https://github.com/pmmmwh/react-refresh-webpack-plugin/blob/HEAD/docs/TROUBLESHOOTING.md#edits-always-lead-to-full-reload) for the `fast-refresh` plugin.

```jsx
import React from 'react';

const MyComponent = () => (
  <h2>Hello</h2>
);

export default MyComponent;
```

### Webpack / Babel

To migrate to v5 you will need to remove your existing `react-hot-loader` and `@hot-loader/react-dom` dependencies and replace them with the new `@pmmmwh/react-refresh-webpack-plugin` and `react-refresh` dependencies. After completing this update your `babel.config.js` or similar to include the required refresh plugin _only in development_:

```js
const isDevelopment = process.env.NODE_ENV !== 'production';

module.exports = {
  presets: ['@babel/preset-env', '@babel/preset-react'],
  plugins: isDevelopment ? ['react-refresh/babel'] : [],
};
```

### React 18

Migrating to fast-refresh for React 18 should be done using the new `createRoot` function, not the legacy `render` function. Coming from React 17 your change might look like the following, which we also completed for our example:

```jsx
import React from 'react';
import ReactDOM from 'react-dom';
import { hot } from 'react-hot-loader/root';
import App from './App';

const HotApp = hot(App);

ReactDOM.render(<HotApp />, document.getElementById('react-root'));
```

changes to:

```jsx
import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';

const container = document.getElementById('react-root');
const root = createRoot(container);
root.render(<App />);
```

### React 16 / 17

Migrating to fast-refresh in React 16 or 17 is fairly straightforward and involves a change reflecting the example below:

```jsx
import React from 'react';
import ReactDOM from 'react-dom';
import { hot } from 'react-hot-loader/root';
import App from './App';

const HotApp = hot(App);

ReactDOM.render(<HotApp />, document.getElementById('react-root'));
```

changes to:

```jsx
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.render(<App />, document.getElementById('react-root'));
```

After this change the babel plugin behind the scenes will handle hot reloading in development.

# Migrating from v3 to v4

Webpack Starter v4 is a large overhaul with an update from Webpack v4 to v5 and many of the recommended default configurations changing. This guide will provide you with a number of steps to help you upgrade your application, and understand why we made these changes to the default configuration.

## Major changes

### Webpack

The [migration from Webpack v4 to v5](https://webpack.js.org/migrate/5/) required many changes, with each loader and configuration needing to be re-evaluated and tested. To simplify the process of evaluating and debugging configuration issues, we have created an [example project](../examples/default) that demonstrates some of the default configurations. If you encounter any issues updating your project to Webpack Starter v4, you can use this project to narrow the scope to provide a reproduceable test case.

The main changes that you will need to make to your project for Webpack 5 are:

* Install the latest version of `webpack@^5.82.0`
* Install the latest version of `webpack-cli@^5.0.2` - This is not required for Webpack Starter, but will be needed for you to run `webpack` or `webpack serve`
* If you are using Webpack Starter's [`addStarterMiddleware`](../README.md#webpack-middleware):
  * Install `webpack-dev-middleware@^6.0.2`
  * Install `webpack-hot-middleware@^2.25.3`
* If you are using Webpack's development server:
  * Install `webpack-dev-server@^4.13.3`
  * Change your script for running the dev server from `webpack-dev-server` to `webpack serve`
* If you have any custom rules using `url-loader` or `file-loader`, you will need to convert them to the new [Asset Modules](https://webpack.js.org/guides/asset-modules/) format, as they are deprecated in Webpack v5

### PostCSS

We no longer provide a default PostCSS configuration, but allow the loader to use your `postcss.config.js`. If you would like to provide the `postcss-loader` with additional options (such as an alternative config path), you can do so through the [styles configuration](https://gitlab.com/vizworx/public/webpack-starter/-/blob/develop/README.md#styles).

The PostCSS configuration that was used in Webpack Starter v3 was as follows, but we recommend reading through the [PostCSS docs](https://github.com/postcss/postcss) and [plugin list](https://github.com/postcss/postcss/blob/main/docs/plugins.md) to configure your own:
```js
const postCSSPresetEnv = require('postcss-preset-env');

module.exports = {
  plugins: [
    postCSSPresetEnv({
      autoprefixer: {
        flexbox: 'no-2009',
      },
    }),
  ],
};
```

### Sass

PostCSS is capable of doing everything that Sass does and more, and we felt it was redundant to have both, so Sass has been removed from the default configuration. If you wish to continue using it, you can install the dependencies and enable it in the [styles configuration](https://gitlab.com/vizworx/public/webpack-starter/-/blob/develop/README.md#styles). To reconfigure it as it was in Webpack Starter v3, you will need to:

* Install `sass-loader@^11.0.1`
* Install `sass@^1.23.0`
* Use the following in your Webpack Starter config:
```js
import webpackStarter from '@vizworx/webpack-starter';
import sassImplementation from 'sass';

export default webpackStarter({
  html: { title: 'My Webpack Project' },
  styles: {
    sass: {
      options: {
        implementation: sassImplementation, // dart-sass
        sassOptions: { fiber: false },
      },
    },
  },
});
```

# Peer Dependencies

Please review the [peer dependencies](../package.json) and any warnings that Yarn gives you, and ensure you have installed the required versions
