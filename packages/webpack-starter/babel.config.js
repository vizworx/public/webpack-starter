module.exports = {
  presets: [
    [
      require.resolve('@babel/preset-env'),
      { useBuiltIns: 'usage', corejs: 3, targets: { node: '14.20.0' } },
    ],
  ],
};
