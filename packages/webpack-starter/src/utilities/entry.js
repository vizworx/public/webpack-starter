export default (entryBase, callback) => {
  if (typeof entryBase === 'object') {
    if (Array.isArray(entryBase)) { return callback(entryBase); }
    const entries = Object.entries(entryBase).map(([k, v]) => ([k, callback([].concat(v))]));
    return Object.fromEntries(entries);
  }
  return callback([].concat(entryBase));
};
