import processEntry from '../utilities/entry';

export default (options) => {
  const config = {
    entry: options.entry,
    resolve: { extensions: ['.jsx'] },
  };

  config.entry = processEntry(options.entry,
    (top) => top.map((v) => (v === './src/index.js' ? './src/index.jsx' : v)));

  return config;
};
