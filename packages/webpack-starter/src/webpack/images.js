export default ({ staticPath, react }) => ({
  module: {
    rules: [
      {
        test: /\.(gif|jpe?g|png)$/,
        type: 'asset',
        ...(!staticPath ? {} : {
          generator: {
            filename: `${staticPath}/[hash][ext][query]`,
          },
        }),
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        ...(react ? { issuer: { not: [/\.(jsx?)$/] } } : {}),
        type: 'asset',
        ...(!staticPath ? {} : {
          generator: {
            filename: `${staticPath}/[hash][ext][query]`,
          },
        }),
      },
    ],
  },
});
