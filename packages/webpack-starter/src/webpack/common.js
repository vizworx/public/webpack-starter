import path from 'path';
import fs from 'fs';
import CaseSensitivePathsPlugin from 'case-sensitive-paths-webpack-plugin';

const appDirectory = fs.realpathSync(process.cwd());

export default () => ({
  entry: './src/index.js',
  output: {
    filename: process.env.NODE_ENV === 'development' ? 'entry~[name].js' : 'entry~[name]~[contenthash].js',
    chunkFilename: process.env.NODE_ENV === 'development' ? 'chunk-[name].js' : 'chunk-[name].[contenthash].js',
    path: path.resolve(appDirectory, 'dist'),
    publicPath: '/',
  },
  resolve: {
    extensions: ['.wasm', '.mjs', '.js', '.json'],
  },
  plugins: [
    new CaseSensitivePathsPlugin(),
  ],
});
