const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');

const isDevelopment = process.env.NODE_ENV !== 'production';

export default (options = {}) => {
  const plugins = [...(options.babel?.plugins || [])];
  const rootPlugins = [];
  if (options.react && options.hot && isDevelopment) {
    plugins.unshift(require.resolve('react-refresh/babel'));
    rootPlugins.unshift(new ReactRefreshWebpackPlugin());
  }

  return {
    module: {
      rules: [
        {
          test: [
            options.react && /\.jsx$/,
            /\.m?js$/,
            /\.wasm$/,
          ],
          exclude: /node_modules/,
          use: {
            loader: require.resolve('babel-loader'),
            options: {
              ...(options.babel || {}),
              plugins,
            },
          },
        },
        // If fullySpecified isn't set, disable the fullySpecified module behavior
        // https://webpack.js.org/configuration/module/#resolvefullyspecified
        options.fullySpecified ? null : {
          test: [/\.m?js$/, /\.wasm$/],
          include: /node_modules/,
          resolve: { fullySpecified: false },

        },
      ].filter((v) => !!v),
    },
    plugins: rootPlugins.filter(Boolean),
  };
};
