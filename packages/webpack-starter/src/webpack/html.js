import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';

export default (options = {}) => ({
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '../static/html.ejs'),
      ...options,
    }),
  ],
});
