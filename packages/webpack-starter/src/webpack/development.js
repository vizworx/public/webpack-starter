export default (options = {}) => ({
  mode: 'development',
  devtool: 'eval-cheap-module-source-map',
  // Display only errors and warnings to reduce the amount of output.
  stats: 'errors-warnings',
  devServer: {
    host: process.env.HOST, // defaults to localhost
    port: process.env.PORT, // defaults to 8080
    historyApiFallback: {
      rewrites: [
        { from: /./, to: '/index.html' },
      ],
    },
    client: {
      overlay: {
        warnings: true,
        errors: true,
      },
    },
    ...options,
  },
});
