import { merge } from 'webpack-merge';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import { WebpackManifestPlugin } from 'webpack-manifest-plugin';

export default (options = {}) => {
  const { lazyChunks = false } = options;
  let config = {
    mode: 'production',
    plugins: [
      new CleanWebpackPlugin(options.clean),
      new WebpackManifestPlugin(options.manifest),
    ],
  };

  if (lazyChunks) {
    const minSize = lazyChunks.minSize || 300 * 1024; // 300KB
    const maxSize = lazyChunks.maxSize || 5 * 1024 * 1024; // 5MB
    config = merge({
      optimization: {
        splitChunks: {
          cacheGroups: {
            'vendors~async': {
              chunks: 'async',
              test: /[\\/]node_modules[\\/]/,
              minSize,
              priority: 10,
            },
            'vendors~other': {
              chunks: 'all',
              test: /[\\/]node_modules[\\/]/,
              minSize,
              maxSize,
              priority: 0,
            },
          },
        },
      },
    });
  }

  return config;
};
