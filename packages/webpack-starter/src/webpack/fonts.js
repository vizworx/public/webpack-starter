export default ({ staticPath }) => ({
  module: {
    rules: [{
      test: /\.(woff2?|ttf|otf|eot)(\?v=\d+\.\d+\.\d+)?$/,
      type: 'asset',
      ...(!staticPath ? {} : {
        generator: {
          filename: `${staticPath}/[hash][ext][query]`,
        },
      }),
    }],
  },
});
