// If you use this, be sure to use it before webpack/images.js
export default () => ({
  module: {
    rules: [{
      test: /\.svg$/,
      issuer: /\.jsx?$/,
      loader: require.resolve('@svgr/webpack'),
    }],
  },
});
