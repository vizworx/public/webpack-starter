import MiniCssExtractPlugin from 'mini-css-extract-plugin';

export default ({ extract = true, postcss = true, sass = false }) => ({
  module: {
    rules: [
      {
        // CSS in node_modules is rendered/extracted, but not parsed
        test: /\.css$/,
        include: /node_modules/,
        use: [
          extract ? MiniCssExtractPlugin.loader : require.resolve('style-loader'),
          require.resolve('css-loader'),
        ],
      },
      {
        test: sass ? /\.(sass|scss|css)$/ : /\.css$/,
        exclude: /node_modules/,
        use: [
          extract ? MiniCssExtractPlugin.loader : require.resolve('style-loader'),
          {
            loader: require.resolve('css-loader'),
            options: {
              // How many loaders before css-loader need to process @imports first
              importLoaders: (postcss ? 1 : 0) + (sass ? 1 : 0),
            },
          },
          // Only load PostCSS if it is enabled
          !postcss ? null : {
            loader: require.resolve('postcss-loader'),
            options: typeof postcss === 'object' ? postcss : {},
          },
          // Only load Sass if it is enabled
          !sass ? null : {
            loader: require.resolve('sass-loader'),
            options: typeof sass === 'object' ? sass : {},
          },
        ].filter((v) => !!v),
      },
    ],
  },
  plugins: [
    extract ? new MiniCssExtractPlugin({
      filename: 'entry~[name]~[contenthash].css',
      chunkFilename: 'chunk-[name].[contenthash].css',
      ...(typeof extract === 'object' ? extract : {}),
    }) : null,
  ].filter((v) => !!v),
});
