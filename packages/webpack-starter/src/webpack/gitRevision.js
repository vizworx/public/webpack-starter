import webpack from 'webpack';
import GitRevisionPlugin from 'git-revision-webpack-plugin';
import { execSync } from 'child_process';

export default () => {
  try {
    execSync('git rev-parse --show-toplevel', { stdio: 'ignore' });
    // If we get to this line, we're in a git directory and can enable the plugin
    return {
      plugins: [
        new webpack.BannerPlugin({
          banner: new GitRevisionPlugin().version(),
        }),
      ],
    };
  } catch {
    // If git threw an error, disable the plugin
    return {};
  }
};
