import path from 'path';
import { merge, mergeWithCustomize, customizeArray, customizeObject } from 'webpack-merge';
import webpack from 'webpack';

import * as parts from './webpack';
import processEntry from './utilities/entry';

export * from './webpack';

const mergeWithReplaceEntry = (...configs) => mergeWithCustomize({
  customizeArray: customizeArray({ 'entry.*': 'replace' }),
  customizeObject: customizeObject({ entry: 'replace' }),
})(...configs);

export default (allOptions = {}) => {
  const {
    html,
    development,
    production = {},
    react = true,
    hot = true,
    entry,
    staticPath = '',
    styles = {},
    gitRevision = true,
    clean,
    manifest,
    babel = {},
    fullySpecified,
    ...remainingOptions
  } = allOptions;

  let config = mergeWithReplaceEntry(
    parts.common(),
    babel ? parts.babel({ babel, fullySpecified, react, hot }) : {},
    parts.fonts({ staticPath }),
    parts.html(html),
    parts.reactSVG(), // This needs to come before images in order for it to work
    parts.images({ staticPath, react }),
  );

  if (react) {
    config = mergeWithReplaceEntry(
      config,
      parts.react({ hot, entry: entry || config.entry }),
    );
  }

  if (process.env.NODE_ENV === 'development') {
    config = merge(
      config,
      parts.development({ hot: !!hot, ...development }),
      parts.styles({ extract: false, ...styles }),
    );
  } else {
    config = merge(
      config,
      gitRevision ? parts.gitRevision() : {},
      parts.production({ clean, manifest, ...production }),
      parts.styles({ extract: true, ...styles }),
    );
  }

  // Merge in all of the user's remaining options at the end
  return merge(config, remainingOptions);
};

export const addStarterMiddleware = (app, webpackConfigBase) => {
  // Only add the middleware in development
  if (process.env.NODE_ENV !== 'development') { return; }

  const entry = processEntry(
    webpackConfigBase.entry,
    (v) => ([require.resolve('webpack-hot-middleware/client')].concat(v)),
  );

  const plugins = (webpackConfigBase.plugins || []).concat(
    new webpack.HotModuleReplacementPlugin(),
  );

  const webpackConfig = {
    ...webpackConfigBase,
    entry,
    plugins,
  };

  /* eslint-disable global-require, import/no-unresolved */
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');
  /* eslint-enable global-require, import/no-unresolved */
  const compiler = webpack(webpackConfig);
  app.use(webpackDevMiddleware(compiler, { publicPath: webpackConfig.output.publicPath }));
  app.use(webpackHotMiddleware(compiler));

  // Generate a SPA fallback for each entrypoint path
  webpackConfigBase.plugins
    .filter((plugin) => (plugin.constructor.name === 'HtmlWebpackPlugin'))
    .map(({ options: { filename } }) => filename)
    .sort((a, b) => b.split('/').length - a.split('/').length) // Sort to ensure nested paths work
    .forEach((htmlPath) => {
      const filename = path.basename(htmlPath);
      if (filename !== 'index.html') { return; }
      // Get the directory name, and trim off the starting ./ if this is the root
      const fallbackMatch = `/${path.dirname(htmlPath)}/*`.replace(/^\/\.\//, '');
      app.get(fallbackMatch, (req, res, next) => {
        const fn = path.join(compiler.outputPath, htmlPath);
        compiler.outputFileSystem.readFile(fn, (err, result) => {
          if (err) { next(err); return; }

          res.set('content-type', 'text/html');
          res.send(result);
          res.end();
        });
      });
    });
};
